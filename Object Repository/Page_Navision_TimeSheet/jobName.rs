<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>jobName</name>
   <tag></tag>
   <elementGuidId>7b1b5b28-e82f-48f9-916d-dc7b545d18ed</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//table[(@summary = 'Time Sheet')]//tbody/tr[${tr}]/td[8]//span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//table[(@summary = 'Time Sheet')]//tbody/tr[${tr}]/td[8]//span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Page_Navision_TimeSheet/form</value>
   </webElementProperties>
</WebElementEntity>
