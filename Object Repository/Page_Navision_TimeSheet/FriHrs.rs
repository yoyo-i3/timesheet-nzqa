<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>FriHrs</name>
   <tag></tag>
   <elementGuidId>118f8979-7644-4246-9324-cfaf610205b7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//table[(@summary = 'Time Sheet')]//tbody/tr[${tr}]/td[13]//span[count(. | //iframe[@ref_element = 'Object Repository/Page_Navision_TimeSheet/iFrame']) = count(//iframe[@ref_element = 'Object Repository/Page_Navision_TimeSheet/iFrame'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//table[(@summary = 'Time Sheet')]//tbody/tr[${tr}]/td[13]//span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Page_Navision_TimeSheet/form</value>
   </webElementProperties>
</WebElementEntity>
