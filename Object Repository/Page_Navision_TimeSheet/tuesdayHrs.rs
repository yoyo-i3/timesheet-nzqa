<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>tuesdayHrs</name>
   <tag></tag>
   <elementGuidId>87cd4bb9-79d7-4756-a5ba-abd328c4eeee</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//table[(@summary = 'Time Sheet')]//tbody/tr[${tr}]/td[10]//span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//table[(@summary = 'Time Sheet')]//tbody/tr[${tr}]/td[10]//span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Page_Navision_TimeSheet/form</value>
   </webElementProperties>
</WebElementEntity>
