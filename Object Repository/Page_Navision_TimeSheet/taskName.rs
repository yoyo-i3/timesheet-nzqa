<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>taskName</name>
   <tag></tag>
   <elementGuidId>0ea7d085-9cf7-428c-8809-a064dd25074b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//table[(@summary = 'Time Sheet')]//tbody/tr[${tr}]/td[7]//span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//table[(@summary = 'Time Sheet')]//tbody/tr[${tr}]/td[7]//span</value>
   </webElementProperties>
</WebElementEntity>
