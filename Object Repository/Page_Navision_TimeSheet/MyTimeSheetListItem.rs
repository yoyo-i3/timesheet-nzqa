<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>MyTimeSheetListItem</name>
   <tag></tag>
   <elementGuidId>92c9d44e-4cf7-4f08-b3f4-065b115eeddb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'ms-nav-navpane-expandcollapse']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@class = 'edit-container']//a[contains(.,'TS')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ms-nav-navpane-expandcollapse</value>
   </webElementProperties>
</WebElementEntity>
