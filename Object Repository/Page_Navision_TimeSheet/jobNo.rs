<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>jobNo</name>
   <tag></tag>
   <elementGuidId>5275f1cb-b3de-40a9-8f2f-6a25c2cfd1a3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//table[(@summary = 'Time Sheet')]//tbody/tr[${tr}]/td[5]//span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//table[(@summary = 'Time Sheet')]//tbody/tr[${tr}]/td[5]//span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Page_Navision_TimeSheet/form</value>
   </webElementProperties>
</WebElementEntity>
