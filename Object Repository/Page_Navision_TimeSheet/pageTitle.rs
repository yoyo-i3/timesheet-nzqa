<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>pageTitle</name>
   <tag></tag>
   <elementGuidId>59094a33-ac63-4921-ac23-4e3067443795</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'contentBox']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@id = 'contentBox']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>contentBox</value>
   </webElementProperties>
</WebElementEntity>
