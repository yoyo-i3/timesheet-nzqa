<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>openTimeSheet</name>
   <tag></tag>
   <elementGuidId>c8c68d6e-5df9-482a-be7e-69e6aa63d0b1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@title, 'Open record &quot;TS')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//span[contains(text(), '${ts} Time Sheet')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Open record &quot;TS</value>
   </webElementProperties>
</WebElementEntity>
