<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>timeSheetTable</name>
   <tag></tag>
   <elementGuidId>6fb2e83e-281b-4c6a-9854-3e8184bf53b1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//iframe[@summary = 'Time Sheet']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>summary</name>
      <type>Main</type>
      <value>Time Sheet</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Page_Navision_TimeSheet/form</value>
   </webElementProperties>
</WebElementEntity>
