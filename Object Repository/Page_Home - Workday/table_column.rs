<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>table_column</name>
   <tag></tag>
   <elementGuidId>e61c6eca-182c-46bf-b7ab-5af964f6eddf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//table[@class='mainTable']//tr[1]//td[${td}]//div//following::div[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//table[@class='mainTable']//tr[1]//td[${td}]//div//following::div[1]</value>
   </webElementProperties>
</WebElementEntity>
