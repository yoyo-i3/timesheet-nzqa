<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>viewChange</name>
   <tag></tag>
   <elementGuidId>96a4f12a-f58f-4c0c-94f9-b9a0c1b9bcfe</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>.//button[@title='Week']//parent::div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>.//button[@title='Week']//parent::div</value>
   </webElementProperties>
</WebElementEntity>
