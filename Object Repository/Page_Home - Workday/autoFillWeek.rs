<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>autoFillWeek</name>
   <tag></tag>
   <elementGuidId>467bf1c0-6c34-4e10-828b-a06b4c923447</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[(text() = 'Auto-fill from Prior Week')]</value>
   </webElementProperties>
</WebElementEntity>
