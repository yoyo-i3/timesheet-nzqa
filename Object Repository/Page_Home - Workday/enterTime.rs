<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>enterTime</name>
   <tag></tag>
   <elementGuidId>0ceb1db8-b4b1-4440-aaf6-5990bf249fd1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div//button[@title='Enter Time']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div//button[@title='Enter Time']</value>
   </webElementProperties>
</WebElementEntity>
