<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Enter Time</name>
   <tag></tag>
   <elementGuidId>12a344c4-dea2-4c3e-9b4c-80919e83d6ca</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>gwt-Label WJNC WOLC WGOC</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-automation-id</name>
      <type>Main</type>
      <value>calendarAppointmentTitle</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Enter Time</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;AppointmentWidgetRoot-uid15&quot;)/div[1]/div[@class=&quot;WLLC WMLC WONC WNLC&quot;]/div[@class=&quot;WJOC&quot;]/div[@class=&quot;gwt-Label WJNC WOLC WGOC&quot;]</value>
   </webElementProperties>
</WebElementEntity>
