import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import javax.media.bean.playerbean.MediaPlayer.popupActionListener as popupActionListener
import javax.swing.ArrayTable as ArrayTable
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.junit.After as After
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.By as By
import org.openqa.selenium.By.ByXPath as ByXPath
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory

WebUI.openBrowser('http://pdnavapp02:8080/DynamicsNAV110/')

WebUI.navigateToUrl('http://pdnavapp02:8080/DynamicsNAV110/')


WebUI.maximizeWindow()

WebUI.switchToFrame(findTestObject('Page_Navision_TimeSheet/Home_iFrame'), 30)
WebUI.waitForElementPresent(findTestObject('Page_Navision_TimeSheet/pageTitle'), 30)

'To get This Week(Open) or Last Week(Approved) - update the below line with required timesheet'

WebUI.click(findTestObject('Page_Navision_TimeSheet/openTimeSheet', ['ts' : 'Submitted']))

WebUI.click(findTestObject('Page_Navision_TimeSheet/MyTimeSheetListItem'))

'To locate table'
WebDriver driver = DriverFactory.getWebDriver()


driver.switchTo().defaultContent()
WebUI.switchToFrame(findTestObject('Page_Navision_TimeSheet/iFrame'), 30)

WebElement Table = driver.findElement(By.xpath('//form[@id=\'aspnetForm\']//table[(@summary = \'Time Sheet\')]//tbody'))

'To locate rows of table it will Capture all the rows available in the table'
List<WebElement> rows_table = Table.findElements(By.tagName('tr'))

'To calculate no of rows In table'
int rows_count = rows_table.size() - 1

println('No. of rows: ' + rows_count)


//workday_time = new String[6][rows_count]
workday_time = new String[rows_count][6]

'Loop will execute for all the rows of the table'
for (int i = 0; i < rows_count; i++) {
    'To locate columns(cells) of that specific row'
    List<WebElement> timesheet_Cols = rows_table.get(i).findElements(By.tagName('td'))

    'To get only td values that contain required info'
    String jobNo = timesheet_Cols.get(4).findElement(By.tagName('span')).getText()

    String taskName = timesheet_Cols.get(6).findElement(By.tagName('a')).getText()

    String monHrs = timesheet_Cols.get(8).findElement(By.tagName('span')).getText()

    String tuesHrs = timesheet_Cols.get(9).findElement(By.tagName('span')).getText()

    String wedHrs = timesheet_Cols.get(10).findElement(By.tagName('span')).getText()

    String thursHrs = timesheet_Cols.get(11).findElement(By.tagName('span')).getText()

    String friHrs = timesheet_Cols.get(12).findElement(By.tagName('span')).getText()
			
		((workday_time[i])[0]) = (((' ' + jobNo) + ' ') + taskName)
	
		((workday_time[i])[1]) = monHrs
	
		((workday_time[i])[2]) = tuesHrs
	
		((workday_time[i])[3]) = wedHrs
	
		((workday_time[i])[4]) = thursHrs
	
		((workday_time[i])[5]) = friHrs
		
	println('workday_time ' + workday_time)
	
}

int arrayLength = workday_time.size()

GlobalVariable.arrayLength = arrayLength

GlobalVariable.workday_time = workday_time

println(GlobalVariable.workday_time)

println(GlobalVariable.arrayLength)

WebUI.closeBrowser()

