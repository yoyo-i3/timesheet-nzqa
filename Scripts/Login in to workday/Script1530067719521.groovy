import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.internal.WebUIKeywordMain as WebUIKeywordMain
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.By as By
import org.openqa.selenium.By.ByTagName as ByTagName
import org.openqa.selenium.By.ByXPath as ByXPath
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import org.testng.internal.thread.ThreadUtil as ThreadUtil
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.JavascriptExecutor as JavascriptExecutor
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import com.kms.katalon.core.webui.contribution.WebUiDriverCleaner as WebUiDriverCleaner

WebUI.openBrowser('https://wd3.myworkday.com/wday/authgwy/assurity/login.htmld')

WebUI.navigateToUrl('https://wd3.myworkday.com/wday/authgwy/assurity/login.htmld')

WebUI.setText(findTestObject('Page_Workday assurity - Sign In to/input_gwt-TextBox GHV1FOUMRB'), Username)

WebUI.setText(findTestObject('Page_Workday assurity - Sign In to/input_gwt-PasswordTextBox GHV1'), Password)

WebUI.click(findTestObject('Page_Workday assurity - Sign In to/button_Sign In'))
WebUI.click(findTestObject('Page_Workday assurity - Sign In to/button_skip'))

WebUI.click(findTestObject('Object Repository/Page_Home - Workday/homePage'))

WebUI.maximizeWindow()

WebUI.click(findTestObject('Object Repository/Page_Home - Workday/time'))

'To enter (This Week) or (Last Week) - update the below line with required week'
WebUI.click(findTestObject('Page_Home - Workday/weekTime', [('enterTime') : 'This Week']))

WebUI.focus(findTestObject('Object Repository/Page_Home - Workday/enterTime'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Page_Home - Workday/enterTime'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Page_Home - Workday/enterTimeType'))

WebUI.waitForElementVisible(findTestObject('Object Repository/Page_Home - Workday/table'), 20)

int rowSize_before = getTableRowCount()

int arrayLength = GlobalVariable.arrayLength

int rows_size_refined = getTableRowCount()

String[] existing_tasks

existing_tasks = new String[rows_size_refined]

int existing_tasks_size

for (int x = 1; x <= rows_size_refined; x++) {
    String timeTypeValue = WebUI.getText(findTestObject('Object Repository/Page_Home - Workday/table_row', [('tr') : x]))

    if (!(timeTypeValue.isEmpty() || timeTypeValue.equalsIgnoreCase(' '))) {
        (existing_tasks[(x - 1)]) = timeTypeValue
    }
}

if (existing_tasks == [null]) {
    existing_tasks_size = 0
} else {
    existing_tasks_size = existing_tasks.size()
}

for (int i = 0; i < arrayLength; i++) {
    WebUI.delay(1)

    'Time Type column'
    setTimeTypeValue(i, existing_tasks_size, existing_tasks)

    'Day Time columns'
    setDayValues(i)
}

WebUI.delay(2)

int rowSize_after = getTableRowCount()

int row_size_diff = rowSize_after - rowSize_before

int final_row_size = arrayLength + existing_tasks_size

println(existing_tasks_size)

println(row_size_diff)

println(final_row_size)

println(rowSize_before)

if (rowSize_after > final_row_size) {
    WebUI.scrollToElement(findTestObject('Object Repository/Page_Home - Workday/deleteRow', [('tr') : final_row_size + 1]), 
        20)

    WebUI.click(findTestObject('Object Repository/Page_Home - Workday/deleteRow', [('tr') : final_row_size + 1]))
}

WebUI.click(findTestObject('Object Repository/Page_Home - Workday/okButton'))

WebUI.waitForElementPresent(findTestObject('Object Repository/Page_Home - Workday/submitButton'), 20, FailureHandling.STOP_ON_FAILURE)

WebUI.closeBrowser()

//	int result = verifyTaskIsPresent(timeTypeValue,rows, existing_tasks)
//	println result
//	
//	if (result != 0){
//		setDayValues(result)
//	}
//	WebUI.delay(5)
'To send keys letter by letter'

private def setTimeTypeValue(int i, int rows, String[] existing_tasks) {
    String timeTypeValue = ((GlobalVariable.workday_time[i])[0]).toString()

    WebUI.click(findTestObject('Object Repository/Page_Home - Workday/addRow'))

    WebUI.delay(1)

    WebUI.click(findTestObject('Object Repository/Page_Home - Workday/table_column', [('td') : 1]))

    WebUI.focus(findTestObject('Object Repository/Page_Home - Workday/table_input', [('td') : 1]))

    WebUI.delay(1)

    WebUI.sendKeys(findTestObject('Object Repository/Page_Home - Workday/table_input', [('td') : 1]), timeTypeValue)

    WebUI.sendKeys(findTestObject('Object Repository/Page_Home - Workday/table_input', [('td') : 1]), Keys.chord(Keys.ENTER))

    WebUI.delay(1)

    WebUI.waitForElementPresent(findTestObject('Object Repository/Page_Home - Workday/timeTypeValue', [('jobNo') : timeTypeValue.substring(
                    0, 5)]), 20, FailureHandling.STOP_ON_FAILURE)
}

private def setDayValues(def i) {
    'Monday column'
    setTableValues(4, i, 1)

    'Tuesday column'
    setTableValues(5, i, 2)

    'Wednesday column'
    setTableValues(6, i, 3)

    'Thursday column'
    setTableValues(7, i, 4)

    'Friday column'
    setTableValues(8, i, 5)
}

private def setTableValues(int td, int i, int j) {
    WebUI.click(findTestObject('Object Repository/Page_Home - Workday/table_column', [('td') : td]))

    if (!(((GlobalVariable.workday_time[i])[j]).equals(''))) {
        WebUI.setText(findTestObject('Object Repository/Page_Home - Workday/table_input', [('td') : td]), (GlobalVariable.workday_time[
            i])[j])
    }
}

private int getTableRowCount() {
    WebDriver driver = DriverFactory.getWebDriver()

    WebElement Table = driver.findElement(By.xpath('//table[@class=\'mainTable\']'))

    List<WebElement> rows_table = Table.findElements(By.tagName('tr'))

    return rows_table.size() - 2
}

private int verifyTaskIsPresent(String timeType_navision, int rows_size, String[] existing_tasks) {
    String[] navision_job_split = timeType_navision.split(' ')

    int arraySize = navision_job_split.size()

    boolean result

    println(timeType_navision)

    println(rows_size)

    println(existing_tasks)

    for (int x; x < rows_size; x++) {
        String timeTypeValue = (existing_tasks[x]).toString().toLowerCase()

        for (int y; y < arraySize; y++) {
            result = timeTypeValue.contains((navision_job_split[y]).toLowerCase())

            println(timeTypeValue)

            println(navision_job_split[y])

            println(result)

            if (result == false) {
                break
            }
        }
        
        if (result == true) {
            if (getTableRowCount() == rows_size) {
                setDayValues(x + 1)
            } else {
                setDayValues(getTableRowCount() - (rows_size - x))
            }
        }
        
        setDayValues(0)
    }
}

private def verifyAllTextMatch(String timeType_navision, String timeTypeValue) {
    String[] navision_job_split = timeType_navision.split(' ')

    int arraySize = navision_job_split.size()

    boolean result = true

    for (int y = 0; (y < arraySize) && (result == true); y++) {
        result = timeTypeValue.contains(navision_job_split[y])

        println(timeTypeValue)

        println(navision_job_split[y])

        println(result)
    }
    
    return result
}

//	for (int x = 0 ; x<rows_size; x++){
//		String timeTypeValue = existing_tasks[x].toString()
//			for(int y =0; y<arraySize && result == true ;y++){
//				result = timeTypeValue.toLowerCase().contains(navision_job_split[y])
//				println timeTypeValue
//				println navision_job_split[y]
//				println result
//			}
//		if(result == true){
//			if(getTableRowCount() == rows_size){
////				return x+1
//				setDayValues(x+1)
//			}else{
////			return getTableRowCount() - (rows_size - x)
//			setDayValues(getTableRowCount() - (rows_size - x))
//			}
//		}
//		setDayValues(0)
//	}
//}
//private int verifyTaskIsPresent(String timeType_navision, int rows_size, String[] existing_tasks){
//	for (int x = 0 ; x<rows_size; x++){
//		String timeTypeValue = existing_tasks[x].toString()
//
//		if(verifyAllTextMatch(timeType_navision, timeTypeValue.toLowerCase()) == true){
//			if(getTableRowCount() == rows_size){
//				return x+1
//			}
//			return getTableRowCount() - (rows_size - x)
//		}
//	}
//	return 0
//}
//private def verifyAllTextMatch(String timeType_navision, String timeTypeValue){
//	StringTokenizer tok = new StringTokenizer(timeType_navision.toLowerCase(), " " )
//	boolean result = true
//	while (tok.hasMoreTokens() && result == true){
//		if(timeTypeValue.contains(tok.nextToken())){
//			println tok.nextToken()
//			println timeTypeValue
//			println result
//			result = true
//		}else {result = false}
//	}
//	return result
//}